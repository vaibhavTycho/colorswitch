﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleControl_ColorSwitch : MonoBehaviour
{
    public bool changeTurn;
    public float spinRate;
    public GameObject increaseScore;
    [SerializeField] private Vector3 lastPos;
    void Start()
    {
        BallControl_ColorSwitch.RetrybuttonClickDelegate += ResetAllVAlues;
        lastPos = transform.position;
    }
    private void ResetAllVAlues()
    {
        transform.position = lastPos;
        increaseScore.SetActive(true);
    }
    void Update()
    {
        if (changeTurn)
        {
            transform.Rotate(0f,0f,-spinRate*Time.deltaTime);
        } else{
            transform.Rotate(0f,0f,spinRate*Time.deltaTime);
        }
     
    }
    private void OnDestroy()
    {
        BallControl_ColorSwitch.RetrybuttonClickDelegate -= ResetAllVAlues;
    }
}
