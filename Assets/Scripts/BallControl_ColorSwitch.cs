﻿using System;
using System.Collections;
using System.Collections.Generic;
using ColorSwitch;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class BallControl_ColorSwitch : MonoBehaviour
{
    public delegate void OnRetryButtonClickDelegate();
    public static OnRetryButtonClickDelegate RetrybuttonClickDelegate;

    private Rigidbody2D rigidbody2D;
    public float bounceForce;
    private int colorNum;
    public Text ScoreText, highScore_text;
    public static int score, highscore;
    public GameObject circle, gameName;
    public Button play, retry, pause;
    [SerializeField] private List<CircleControl_ColorSwitch> circles;
    [SerializeField] private float OffsetPosition, ypos;
    private int firstIndex = 0;
    public static bool isBallPlaying = false;
    [SerializeField] private Vector3 lastPos;
    private void Awake()
    {
        setRandomColor();
        ScoreText.text = score.ToString();
        isBallPlaying = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        retry.gameObject.SetActive(false);
        // Time.timeScale = 0;
        score = 0;
        rigidbody2D = GetComponent<Rigidbody2D>();
        // isBallPlaying = false;
        SetBallBodyType(RigidbodyType2D.Kinematic);
        lastPos = transform.position;
        highscore = PlayerPrefs.GetInt("highscore",0);
        highScore_text.text = highscore.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space) )&& isBallPlaying)
        {
            rigidbody2D.velocity = Vector2.up * bounceForce;
        }
    }
    public  void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "IncreaseScore")
        {
            score += 50;
            ScoreText.text = score.ToString();
            other.gameObject.SetActive(false);
            ChangeCirclePos();
            HighScoreUpdate();
        }
    }
   
    private void HighScoreUpdate()
    {
        if (score >= highscore)
        {
            highscore = score;
            highScore_text.text = highscore.ToString();
            PlayerPrefs.SetInt("highscore", highscore);
        }
    }
    public void OnTriggerExit2D(Collider2D other)
    {
        if (transform.tag != other.tag
            && other.tag != "IncreaseScore" && isBallPlaying)
        {
            Debug.Log("destroy ball");
            isBallPlaying = false;
            ScoreText.text = score.ToString();
            SetVelocity();
            retry.gameObject.SetActive(true);
        }
        else
        {
            if (other.tag != "IncreaseScore")
                setRandomColor();
        }
    }
    public void Retry()
    {
       // SceneManager.LoadScene("ColorSwitch");
        OnRetryButtonClick();
        SetVelocity();
        SetBallBodyType(RigidbodyType2D.Kinematic);
        score = 00;
        firstIndex = 0;
        ScoreText.text = score.ToString();
        pause.gameObject.SetActive(true);
    }

    public void Play()
    {
        // Time.timeScale = 1;
        // rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
        gameName.SetActive(false);
        SetBallBodyType(RigidbodyType2D.Dynamic);
        isBallPlaying = true;
        play.gameObject.SetActive(false);
        pause.gameObject.SetActive(true);
    }
    private void setRandomColor()
    {
      //  Debug.Log("<color=pink> Set color </color>");
        colorNum = CircleColor_ColorSwitch.getRandomColor();
        Color color = CircleColor_ColorSwitch.getColor(colorNum);

        GetComponent<SpriteRenderer>().color = color;
        transform.tag = CircleColor_ColorSwitch.getColorName(colorNum);
    }
   
    private void ChangeCirclePos()
    {
        firstIndex = (firstIndex < circles.Count) ? firstIndex : (firstIndex % circles.Count);
     /*   Debug.Log(firstIndex+ "  "+circles[firstIndex].gameObject.transform.position.y + "  " + (transform.position.y + OffsetPosition));
        Debug.Log("first index " + firstIndex);*/
        if (circles[firstIndex].gameObject.transform.position.y <= (transform.position.y + OffsetPosition))
        {
            circles[firstIndex].gameObject.transform.position = transform.position + Vector3.up * ypos;
            circles[firstIndex].increaseScore.SetActive(true);
            firstIndex++;
        }
    }
    public void SetBallBodyType(RigidbodyType2D type2D)
    {
         rigidbody2D.bodyType = type2D;
    }
    public void SetVelocity()
    {
        rigidbody2D.velocity = Vector3.zero;
        rigidbody2D.angularVelocity = 0;
    }
  
    public void OnRetryButtonClick()
    {
        transform.position = lastPos;
        RetrybuttonClickDelegate();
    }
}