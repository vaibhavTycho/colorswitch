﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBall_ColorSwitch : MonoBehaviour
{
    public Transform ballTransform;
    private Vector3 lastPos;
    // Start is called before the first frame update
    void Start()
    {
        BallControl_ColorSwitch.RetrybuttonClickDelegate += ResetCamera;
        lastPos = transform.position;
    }
    private void ResetCamera()
    {
        transform.position = lastPos;
    }
    // Update is called once per frame
    void Update()
    {
        if (ballTransform.position.y > transform.position.y)
        {
            transform.position=new Vector3(transform.position.x,ballTransform.position.y,transform.position.z);
        }
    }
    private void OnDestroy()
    {
        BallControl_ColorSwitch.RetrybuttonClickDelegate -= ResetCamera;

    }
}
