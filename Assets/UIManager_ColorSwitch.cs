using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager_ColorSwitch : MonoBehaviour
{
    [SerializeField] private BallControl_ColorSwitch ball;
    [SerializeField] private GameObject playBtn, PauseBtn,retryBtn;
    private void Start()
    {
        BallControl_ColorSwitch.RetrybuttonClickDelegate += ResetButtons;
    }
    private void ResetButtons()
    {
        playBtn.SetActive(true);
        retryBtn.SetActive(false);
        PauseBtn.SetActive(true);
    }
    public void Play()
    {
        SceneManager.LoadScene("ColorSwitch");
        PauseBtn.SetActive(true);
    }
    public void Resume(int value)
    {
        var temp = value == 1 ? true : false;
        // Time.timeScale = value;
        playBtn.SetActive(!temp);
        PauseBtn.SetActive(temp);
        var type = value==1 ? RigidbodyType2D.Dynamic : RigidbodyType2D.Kinematic;
        ball.SetBallBodyType(type);
        if(!temp)
         ball.SetVelocity();
    }
    private void OnDestroy()
    {
        BallControl_ColorSwitch.RetrybuttonClickDelegate -= ResetButtons;
    }
}
