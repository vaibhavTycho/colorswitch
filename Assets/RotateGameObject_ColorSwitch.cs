using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateGameObject_ColorSwitch : MonoBehaviour
{
    [SerializeField]private bool left,rotateOn;
    [SerializeField] private float offSet;
    private void Update()
    {
        if(rotateOn)
        {
            if (left)
                transform.Rotate(Vector3.forward, offSet);
            else
                transform.Rotate(Vector3.forward,-offSet );

        }
    }
}
